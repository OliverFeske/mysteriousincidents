// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MCMasterActorComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYSTERIOUSINCIDENTS_API UMCMasterActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMCMasterActorComponent();

public:
	void SetCustomOwner(class AMainCharacter* CustomOwner) { OwningPlayer = CustomOwner; }

protected:
	// Component Getters
	class UMCInteractionComponent* GetInteractionComponent();
	class UMCInventoryComponent* GetInventoryComponent();
	class UMCMovementComponent* GetAdvancedMovementComponent();
	class UMCUIComponent* GetUIComponent();
	class UCapsuleComponent* GetItemCollisionCapsule();
	class UCameraComponent* GetCamera();
	class AMainCharacter* GetCustomOwner() { return OwningPlayer; }

	class AMainCharacter* OwningPlayer{ nullptr };
};