// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MCMasterActorComponent.h"
#include "MCInventoryComponent.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UMCInventoryComponent : public UMCMasterActorComponent
{
	GENERATED_BODY()

public:
	UMCInventoryComponent() {}

public:
	void AddNewItem(class APickUpItem* NewItem) { Items.Add(NewItem); UE_LOG(LogTemp, Warning, TEXT("Added Item")); }
	TArray<class APickUpItem*> GetItems() { return Items; }

private:
	TArray<class APickUpItem*> Items;
};
