// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "ToggleMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UToggleMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
		void CloseMenu();
	virtual void Setup() override;

protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_CloseMenu;
};
