// Fill out your copyright notice in the Description page of Project Settings.


#include "StopDialog.h"

#include "NPCAIController.h"

EBTNodeResult::Type UStopDialog::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto AIController = OwnerComp.GetAIOwner();
	if (!ensure(AIController != nullptr)) return EBTNodeResult::Failed;

	auto BrainComponent = AIController->BrainComponent;
	if (!ensure(BrainComponent != nullptr)) return EBTNodeResult::Failed;

	BrainComponent->StopLogic(TEXT("This is for Dialog logic, so we need to stop or pause it because otherwise we would continuously loop a sequence."));

	return EBTNodeResult::Succeeded;
}