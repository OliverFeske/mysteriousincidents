// Fill out your copyright notice in the Description page of Project Settings.

#include "MCInteractionComponent.h"

#include "Components/CapsuleComponent.h"

#include "MCInventoryComponent.h"
#include "PickUpItem.h"
#include "NPC.h"
#include "DialogMenuWidget.h"
#include "MainCharacter.h"

void UMCInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	GetItemCollisionCapsule()->OnComponentBeginOverlap.AddDynamic(this, &UMCInteractionComponent::OnBeginOverlap);
	GetItemCollisionCapsule()->OnComponentEndOverlap.AddDynamic(this, &UMCInteractionComponent::OnEndOverlap);
}
	
void UMCInteractionComponent::OnBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IInteractable* OverlappedItem = Cast<IInteractable>(OtherActor);
	if (OverlappedItem != nullptr)
		AddNewInteractableInRange(OverlappedItem);
}

void UMCInteractionComponent::OnEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	IInteractable* OverlappedItem = Cast<IInteractable>(OtherActor);
	if (OverlappedItem != nullptr)
		RemoveInteractableInRange(OverlappedItem);
}

void UMCInteractionComponent::TryInteract()
{
	if (InteractablesInRange.Num() <= 0) return;

	IInteractable* NearestInteractable = GetNearestInteractable();
	if (!ensure(NearestInteractable != nullptr)) return;

	NearestInteractable->Interact(GetCustomOwner());
}

void UMCInteractionComponent::AddNewInteractableInRange(IInteractable* Interactable)
{
	InteractablesInRange.Add(Interactable);

	Interactable->OnInRange();
}

void UMCInteractionComponent::RemoveInteractableInRange(IInteractable* Interactable)
{
	int32 Idx = InteractablesInRange.Find(Interactable);
	if (Idx != INDEX_NONE)
	{
		InteractablesInRange.RemoveAt(Idx);

		Interactable->OnOutOfRange();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Couldn't remove Interactable: %s at Index: %i"), *Interactable->GetName(), Idx);
	}
}

IInteractable* UMCInteractionComponent::GetNearestInteractable()
{
	IInteractable* NearestInteractable{ nullptr };
	float Distance{ INFINITY };

	for (auto Interactable : InteractablesInRange)
	{
		AActor* Owner = GetOwner();
		if (!ensure(Owner != nullptr)) continue;

		AActor* InteractableOwner = Cast<AActor>(Interactable);
		if (!ensure(InteractableOwner != nullptr)) return nullptr;

		float NewDist = FVector::Dist(Owner->GetActorLocation(), InteractableOwner->GetActorLocation());

		if (NewDist < Distance)
			NearestInteractable = Interactable;
	}

	return NearestInteractable;
}