// Fill out your copyright notice in the Description page of Project Settings.


#include "DisplayDialogText.h"

#include "Kismet/GameplayStatics.h"

#include "MysteriousIncidentsGameInstance.h"
#include "DialogMenuWidget.h"

EBTNodeResult::Type UDisplayDialogText::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return EBTNodeResult::Failed;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return EBTNodeResult::Failed;

	UToggleMenuWidget* ToggleMenu = Inst->GetDialogWidget();
	if (!ensure(ToggleMenu != nullptr)) return EBTNodeResult::Failed;

	UDialogMenuWidget* DialogWidget = Cast<UDialogMenuWidget>(ToggleMenu);
	if (!ensure(DialogWidget != nullptr)) return EBTNodeResult::Failed;

	FString AppendedString{ "" };
	for (auto Text : DialogTextParts)
	{
		AppendedString += Text + "\n";
	}

	DialogWidget->SetText(AppendedString);

    return EBTNodeResult::Succeeded;
}