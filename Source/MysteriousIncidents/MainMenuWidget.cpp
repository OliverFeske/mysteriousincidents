// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuWidget.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"

#include "Components/WidgetSwitcher.h"

bool UMainMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Button_BackFromLoad != nullptr)) return false;
	Button_BackFromLoad->OnClicked.AddDynamic(this, &UMainMenuWidget::SwitchToStartingMenu);

	if (!ensure(Button_SwitchToLoadMenu != nullptr)) return false;
	Button_SwitchToLoadMenu->OnClicked.AddDynamic(this, &UMainMenuWidget::SwitchToLoadMenu);

	if (!ensure(Button_QuitApplication != nullptr)) return false;
	Button_QuitApplication->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitApplication);

	if (!ensure(Button_StartNewGame != nullptr)) return false;
	Button_StartNewGame->OnClicked.AddDynamic(this, &UMainMenuWidget::StartNewGame);

	return true;
}

void UMainMenuWidget::OnLevelRemovedFromWorld(ULevel* Level, UWorld* W)
{
	Super::OnLevelRemovedFromWorld(Level, W);

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;

	FInputModeGameOnly InputModeData;

	PC->SetInputMode(InputModeData);
	PC->bShowMouseCursor = false;
}

void UMainMenuWidget::SwitchToLoadMenu()
{
	if (!ensure(MenuSwitcher != nullptr)) return;
	if (!ensure(LoadMenu != nullptr)) return;

	MenuSwitcher->SetActiveWidget(LoadMenu);
}

void UMainMenuWidget::SwitchToStartingMenu()
{
	if (!ensure(MenuSwitcher != nullptr)) return;
	if (!ensure(StartingMenu != nullptr)) return;

	MenuSwitcher->SetActiveWidget(StartingMenu);
}

void UMainMenuWidget::QuitApplication()
{
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;

	PC->ConsoleCommand("Quit");
}

void UMainMenuWidget::StartNewGame()
{
	if (!ensure(MenuInterface != nullptr)) return;

	MenuInterface->LoadLevelAsync("Island");
}

void UMainMenuWidget::LoadSavedGame()
{
	UE_LOG(LogTemp, Error, TEXT("LOADING NOT SUPPORTED YET!"));

	//if (!ensure(MenuInterface != nullptr)) return;

	//MenuInterface->LoadMainLevel("");
}