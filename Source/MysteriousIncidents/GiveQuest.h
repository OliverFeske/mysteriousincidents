// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

#include "StructCollection.h"

#include "GiveQuest.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UGiveQuest : public UBTTaskNode
{
	GENERATED_BODY()
	// GET THE PLAYER REF FROM THE BLACKBOARD AND ASSIGN AN OBJECTIVE TO HIM

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UPROPERTY(EditAnywhere)
		FQuestBase Quest;
};
