// Fill out your copyright notice in the Description page of Project Settings.

#include "NPCManager.h"

#include "NPCDialogTableStructure.h"
#include "NPC.h"

#include "Engine/DataTable.h"

// Sets default values for this component's properties
ANPCManager::ANPCManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts
void ANPCManager::BeginPlay()
{
	Super::BeginPlay();
}