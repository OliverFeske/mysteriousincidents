// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "PrepareBlackboard.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UPrepareBlackboard : public UBTTaskNode
{
	GENERATED_BODY()
	
		// PREPARE THE BLACKBOARD BY SETTING ITS BASIC VALUES LIKE E.G. THE PLAYER
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	// The index of the array is the the ID of the dialog option. Put the default value of their lock state in here according to the ID
	UPROPERTY(EditAnywhere)
		TArray<bool> DialogOptionIDs;
};
