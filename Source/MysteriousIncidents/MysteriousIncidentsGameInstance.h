// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "MenuInterface.h"

#include "MysteriousIncidentsGameInstance.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UMysteriousIncidentsGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	// UI
	UFUNCTION(BlueprintCallable)
		void LoadMainMenuWidget();
	UFUNCTION(BlueprintCallable)
		void LoadEndScreenWidget();
	UFUNCTION(BlueprintCallable)
		void OpenOptionsMenuWidget();
	UFUNCTION(BlueprintCallable)
		void OpenInventoryMenuWidget();
	UFUNCTION(BlueprintCallable)
		void LoadLoadingWidget();

	UFUNCTION(BlueprintCallable)
		class ANPCManager* GetCurrentNPCManager() { return CurrentNPCManager; }
	UFUNCTION(BlueprintCallable)
		void SetCurrentNPCManager(class ANPCManager* NewNPCManager) { CurrentNPCManager = NewNPCManager; }
	UFUNCTION(BlueprintCallable)
		class AItemManager* GetCurrentItemManager() { return CurrentItemManager; }
	UFUNCTION(BlueprintCallable)
		void SetCurrentItemManager(class AItemManager* NewItemManager) { CurrentItemManager = NewItemManager; }
	UFUNCTION(BlueprintCallable)
		class AQuestManager* GetCurrentQuestManager() { return CurrentQuestManager; }
	UFUNCTION(BlueprintCallable)
		void SetCurrentQuestManager(class AQuestManager* NewQuestmanager) { CurrentQuestManager = NewQuestmanager; }

	class UDialogMenuWidget* GetDialogWidget() { return DialogMenu; }

	void CreateOptionsMenuWidget();
	void CreateInventoryMenuWidget();
	void CreateDialogMenuWidget();

	// Loading
	virtual void LoadLevelAsync(FString LevelName) override;

	void StartLoadingPackage();
	void OnLoadPackageSucceeded();
	UFUNCTION()
		void OnPreheatFinished();

	UPROPERTY()
		float MasterVolume{ 0.0f };
	UPROPERTY()
		float MusicVolume{ 0.0f };
	UPROPERTY()
		float AmbientVolume{ 0.0f };
	UPROPERTY()
		float SFXVolume{ 0.0f };

private:
	class UToggleMenuWidget* CreateToggleMenuWidget(class UToggleMenuWidget* MemberPtr, TSubclassOf<class UToggleMenuWidget> ToggleMenuClass);

	UPROPERTY(VisibleAnywhere)
		class AMainCharacter* MainCharacter{ nullptr };
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UMenuWidget> MainMenuClass;
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UMenuWidget> EndScreenClass;
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UToggleMenuWidget> OptionsMenuClass;
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UToggleMenuWidget> InventoryMenuClass;
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UToggleMenuWidget> DialogMenuClass;
	UPROPERTY(EditAnywhere, Category = "UIClasses")
		TSubclassOf<class UUserWidget> LoadingScreenClass;

	UPROPERTY()
		class UOptionsMenuWidget* OptionsMenu{ nullptr };
	UPROPERTY()
		class UInventoryMenuWidget* InventoryMenu{ nullptr };
	UPROPERTY()
		class UDialogMenuWidget* DialogMenu{ nullptr };
	UPROPERTY()
		class UMainMenuWidget* MainMenu{ nullptr };
	UPROPERTY()
		class UEndScreenWidget* EndScreen{ nullptr };

	UPROPERTY()
		class ANPCManager* CurrentNPCManager{ nullptr };
	UPROPERTY()
		class AItemManager* CurrentItemManager{ nullptr };
	UPROPERTY()
		class AQuestManager* CurrentQuestManager{ nullptr };

	UPROPERTY()
		FTimerHandle hLoadingTimer;
	UPROPERTY()
		FString CurrentLoadingLevelName{ "" };
	UPROPERTY()
		bool bIsCurrentlyLoading{ false };
	UPROPERTY()
		bool bIsInventoryMenuActive = false;
};
