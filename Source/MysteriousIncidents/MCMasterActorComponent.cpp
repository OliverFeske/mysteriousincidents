// Fill out your copyright notice in the Description page of Project Settings.

#include "MCMasterActorComponent.h"

#include "MainCharacter.h"

// Sets default values for this component's properties
UMCMasterActorComponent::UMCMasterActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame. You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

UMCInteractionComponent* UMCMasterActorComponent::GetInteractionComponent()
{ 
	return OwningPlayer->GetInteractionComponent(); 
}

UMCInventoryComponent* UMCMasterActorComponent::GetInventoryComponent()
{ 
	return OwningPlayer->GetInventoryComponent(); 
}

UMCMovementComponent* UMCMasterActorComponent::GetAdvancedMovementComponent()
{ 
	return OwningPlayer->GetAdvancedMovementComponent(); 
}

UCapsuleComponent* UMCMasterActorComponent::GetItemCollisionCapsule()
{ 
	return OwningPlayer->GetItemCollisionCapsule(); 
}

UCameraComponent* UMCMasterActorComponent::GetCamera()
{ 
	return OwningPlayer->GetCamera(); 
}