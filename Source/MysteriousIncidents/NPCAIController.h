// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "NPCAIController.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API ANPCAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	TArray<class UNPCDialogBT*>& GetBehaviorTrees() { return BehaviorTrees; }

private:
	UPROPERTY(EditAnywhere)
		TArray<class UNPCDialogBT*> BehaviorTrees;
};