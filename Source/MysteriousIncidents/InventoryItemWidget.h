// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "InventoryItemWidget.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UInventoryItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Setup(class UInventoryMenuWidget* InParent, uint8 Idx, class APickUpItem* Item);

protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
		void SelectItem();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_SelectItem;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_ItemName;

	class UInventoryMenuWidget* Parent;

	uint8 Index;
};
