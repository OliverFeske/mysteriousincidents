// Fill out your copyright notice in the Description page of Project Settings.


#include "GiveQuest.h"

#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "MysteriousIncidentsGameInstance.h"
#include "QuestManager.h"

EBTNodeResult::Type UGiveQuest::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto Blackboard = OwnerComp.GetBlackboardComponent();
	if (!ensure(Blackboard != nullptr)) return EBTNodeResult::Failed;

	FString IString = FString::FromInt(Quest.QuestID);
	FString Key = "IsLocked_Quest_" + IString;

	if (Blackboard->GetValueAsBool(FName(*Key)) == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Skipped Quest Creation: %s"), *Key);
		return EBTNodeResult::Succeeded;
	}

	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return EBTNodeResult::Failed;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return EBTNodeResult::Failed;

	AQuestManager* QuestManager = Inst->GetCurrentQuestManager();
	if (!ensure(QuestManager != nullptr)) return EBTNodeResult::Failed;

	Blackboard->SetValueAsBool(FName(*Key), true);

	QuestManager->CreateQuest(Quest);

	return EBTNodeResult::Succeeded;
}