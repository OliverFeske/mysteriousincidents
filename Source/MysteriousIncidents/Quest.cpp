// Fill out your copyright notice in the Description page of Project Settings.

#include "Quest.h"

#include "Kismet/GameplayStatics.h"

#include "MysteriousIncidentsGameInstance.h"
#include "ItemManager.h"
#include "NPCManager.h"
#include "QuestManager.h"
#include "NPC.h"
#include "PickUpItem.h"

void UQuest::Setup(const FQuestBase& InQuestBase, class UMysteriousIncidentsGameInstance* InstRef)
{
	QuestBase = InQuestBase;

	Inst = InstRef;

	StartObjective();
}

bool UQuest::CountDownItems(class APickUpItem* InItem)
{
	if (!ensure(InItem != nullptr)) return false;

	if (!ensure(Inst != nullptr)) return false;

	auto ItemManager = Inst->GetCurrentItemManager();
	if (!ensure(ItemManager != nullptr)) return false;

	if (ObjectiveItems.Find(InItem) != INDEX_NONE)
	{
		ObjectiveItems.Remove(InItem);
		CheckObjectiveFinished();

		return true;
	}

	return false;
}

bool UQuest::CountDownObjectiveCharacters(ANPC* InNPC, uint8 InDialogID)
{
	if (ObjectiveCharacterDialogs.Num() <= 0) return false;

	if (!ensure(InNPC != nullptr)) return false;

	if (!ensure(Inst != nullptr)) return false;

	auto ItemManager = Inst->GetCurrentItemManager();
	if (!ensure(ItemManager != nullptr)) return false;

	for (size_t i = 0; i < ObjectiveCharacterDialogs.Num(); i++)
	{
		if (ObjectiveCharacterDialogs[i].NPC == InNPC || ObjectiveCharacterDialogs[i].DialogID == InDialogID)
		{
			ObjectiveCharacterDialogs.RemoveAt(i);
			CheckObjectiveFinished();

			return true;
		}
	}

	return false;
}

void UQuest::UnlockObjectiveItems(const TArray<uint8>& ItemsToUnlock)
{
	if (!ensure(Inst != nullptr)) return;

	auto ItemManager = Inst->GetCurrentItemManager();
	if (!ensure(ItemManager != nullptr)) return;

	auto Items = ItemManager->GetLevelItems();

	for (auto ItemID : ItemsToUnlock)
	{
		if (Items.IsValidIndex(ItemID))
			if (!ensure(Items[ItemID] != nullptr))
				continue;
			else
			{
				Items[ItemID]->SetupAnswer(this);
				Items[ItemID]->Unlock();
				ObjectiveItems.Add(Items[ItemID]);
			}
	}
}

void UQuest::UnlockItems(const TArray<uint8>& ItemsToUnlock)
{
	if (!ensure(Inst != nullptr)) return;

	auto ItemManager = Inst->GetCurrentItemManager();
	if (!ensure(ItemManager != nullptr)) return;

	auto Items = ItemManager->GetLevelItems();

	for (auto ItemID : ItemsToUnlock)
	{
		if (Items.IsValidIndex(ItemID))
		{
			if (!ensure(Items[ItemID] != nullptr))
				continue;
			else
			{
				Items[ItemID]->Unlock();
			}
		}
	}
}

void UQuest::UnlockDialogs(const TArray<FCharacterDialog>& DialogsToUnlock)
{
	if (!ensure(Inst != nullptr)) return;

	auto NPCManager = Inst->GetCurrentNPCManager();
	if (!ensure(NPCManager != nullptr)) return;

	auto NPCs = NPCManager->GetNPCs();

	for (auto DialogToUnlock : DialogsToUnlock)
	{
		if (NPCs.IsValidIndex(DialogToUnlock.Character_ID))
		{
			auto NPC = NPCs[DialogToUnlock.Character_ID];
			if (!ensure(NPC != nullptr)) continue;

			NPC->UnlockDialog(DialogToUnlock.Dialog_ID);
		}
	}
}

void UQuest::UnlockObjectiveDialogs(const TArray<FCharacterDialog>& DialogsToUnlock)
{
	if (!ensure(Inst != nullptr)) return;

	auto NPCManager = Inst->GetCurrentNPCManager();
	if (!ensure(NPCManager != nullptr)) return;

	auto NPCs = NPCManager->GetNPCs();

	for (auto DialogToUnlock : DialogsToUnlock)
	{
		if (NPCs.IsValidIndex(DialogToUnlock.Character_ID))
		{
			auto NPC = NPCs[DialogToUnlock.Character_ID];
			if (!ensure(NPC != nullptr)) continue;

			NPC->UnlockDialog(DialogToUnlock.Dialog_ID);
			NPC->SetUpAnswer(this);

			FObjectiveCharacter NewObjectiveCharacter;
			NewObjectiveCharacter.DialogID = DialogToUnlock.Dialog_ID;
			NewObjectiveCharacter.NPC = NPC;

			ObjectiveCharacterDialogs.Add(NewObjectiveCharacter);
		}
	}
}

void UQuest::StartObjective()
{
	if (CurrentObjectiveIndex >= QuestBase.ObjectivesInOrder.Num())
	{
		OnQuestFinished();
	}

	if (!QuestBase.ObjectivesInOrder.IsValidIndex(CurrentObjectiveIndex)) return;

	UnlockObjectiveItems(QuestBase.ObjectivesInOrder[CurrentObjectiveIndex].ObjectiveItems);
	UnlockObjectiveDialogs(QuestBase.ObjectivesInOrder[CurrentObjectiveIndex].ObjectiveDialogs);
}

void UQuest::CheckObjectiveFinished()
{
	if (ObjectiveItems.Num() <= 0 || ObjectiveCharacterDialogs.Num() <= 0)
		OnObjectiveFinished();
}

void UQuest::OnObjectiveFinished()
{
	if (!QuestBase.ObjectivesInOrder.IsValidIndex(CurrentObjectiveIndex)) return;

	//UnlockItems(QuestBase.ObjectivesInOrder[CurrentObjectiveIndex].ItemsToUnlockOnObjectiveFinished);
	//UnlockDialogs(QuestBase.ObjectivesInOrder[CurrentObjectiveIndex].DialogsToUnlockOnObjectiveFinished);

	CurrentObjectiveIndex++;

	StartObjective();

}

void UQuest::OnQuestFinished()
{
	if (!ensure(Inst != nullptr)) return;

	auto QuestManager = Inst->GetCurrentQuestManager();
	if (!ensure(QuestManager != nullptr)) return;

	QuestManager->FinishedQuest(this);
}