// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"

#include "MCInteractionComponent.h"
#include "MCInventoryComponent.h"
#include "MCMovementComponent.h"

AMainCharacter::AMainCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ItemCollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ItemCollisionCapsule"));
	ItemCollisionCapsule->SetupAttachment(RootComponent);
	ItemCollisionCapsule->InitCapsuleSize(50.0f, 95.0f);
	ItemCollisionCapsule->SetCollisionProfileName("Interactable");
	ItemCollisionCapsule->SetRelativeLocation(FVector(30.0f, 0.0f, 0.0f));
	ItemCollisionCapsule->SetNotifyRigidBodyCollision(true);

	InteractionComponent = CreateDefaultSubobject<UMCInteractionComponent>(TEXT("InteractionComponent"));
	InteractionComponent->SetCustomOwner(this);

	InventoryComponent = CreateDefaultSubobject<UMCInventoryComponent>(TEXT("UMCInventoryComponent"));
	InventoryComponent->SetCustomOwner(this);

	AdvancedMovementComponent = CreateDefaultSubobject<UMCMovementComponent>(TEXT("AdvancedMovementComponent"));
	AdvancedMovementComponent->SetCustomOwner(this);
}

void AMainCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	AMysteriousIncidentsCharacter::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::StopSprint);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, InteractionComponent, &UMCInteractionComponent::TryInteract);
}

void AMainCharacter::MoveForward(float Value)
{
	if (!ensure(AdvancedMovementComponent != nullptr)) return;

	AdvancedMovementComponent->SetDirectionX(Value);
}

void AMainCharacter::MoveRight(float Value)
{
	if (!ensure(AdvancedMovementComponent != nullptr)) return;

	AdvancedMovementComponent->SetDirectionY(Value);
}

void AMainCharacter::Sprint()
{
	AdvancedMovementComponent->SetCanSprint(true);
}

void AMainCharacter::StopSprint()
{
	AdvancedMovementComponent->SetCanSprint(false);
}