// Fill out your copyright notice in the Description page of Project Settings.


#include "MCMovementComponent.h"

#include "MainCharacter.h"

void UMCMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// dont do anything if there is no input
	if (MovementDirection == FVector::ZeroVector) return;

	// if there is double input calculate normal direction because otherwise it would speed up the character
	if (MovementDirection.X != 0.0f && MovementDirection.Y != 0.0f)
		if (MovementDirection.SizeSquared() > 1.0f)
			MovementDirection.Normalize();

	// X
	if (GetCustomOwner()->Controller != nullptr && MovementDirection.X != 0.0f)
	{
		if (!bCanSprint)
			MovementDirection.X *= 0.3f;

		// find out which way is forward
		const FRotator Rotation = GetCustomOwner()->Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		GetCustomOwner()->AddMovementInput(Direction, MovementDirection.X);
	}

	// Y
	if (GetCustomOwner()->Controller != nullptr && MovementDirection.Y != 0.0f)
	{
		if (!bCanSprint)
			MovementDirection.Y *= 0.3f;

		// find out which way is right
		const FRotator Rotation = GetCustomOwner()->Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		GetCustomOwner()->AddMovementInput(Direction, MovementDirection.Y);
	}
}