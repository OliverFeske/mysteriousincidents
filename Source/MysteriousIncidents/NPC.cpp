// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC.h"

#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/GameplayStatics.h"
#include "MysteriousIncidentsGameInstance.h"
#include "GameFramework/Controller.h"
#include "AIController.h"

#include "DialogMenuWidget.h"
#include "NPCAIController.h"
#include "NPCDialogBT.h"
#include "Quest.h"

// Sets default values
ANPC::ANPC()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CharacterCollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CharacterCollisionCapsule"));
	CharacterCollisionCapsule->InitCapsuleSize(60.0f, 100.0f);
	CharacterCollisionCapsule->CanCharacterStepUpOn = ECB_No;
	CharacterCollisionCapsule->SetupAttachment(RootComponent);
	CharacterCollisionCapsule->SetShouldUpdatePhysicsVolume(true);
	CharacterCollisionCapsule->SetCanEverAffectNavigation(false);
	CharacterCollisionCapsule->bDynamicObstacle = false;
	CharacterCollisionCapsule->SetCollisionProfileName(FName(TEXT("Interactable")));
}

// Called when the game starts or when spawned
void ANPC::BeginPlay()
{
	Super::BeginPlay();

	// Always unlock the first dialog tree
	UnlockDialog(0);
}

void ANPC::UnlockDialog(uint8 Idx)
{
	if (Dialogs.IsValidIndex(Idx))
		UnlockedDialogs.AddUnique(Dialogs[Idx]);
}

void ANPC::Interact(AMainCharacter* MainCharacter)
{
	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return;

	Inst->CreateDialogMenuWidget();

	auto DialogWidget = Inst->GetDialogWidget();
	if (!ensure(DialogWidget != nullptr)) return;

	DialogWidget->SetNPC(this);
}

void ANPC::RunCurrentBehaviorTree(uint8 Idx)
{
	AController* CurrentController = GetController();
	if (!ensure(CurrentController != nullptr)) return;

	auto AIController = Cast<AAIController>(CurrentController);
	if (!ensure(AIController != nullptr)) return;

	CurrentAIController = AIController;
	if (!ensure(CurrentAIController != nullptr)) return;

	if (UnlockedDialogs.IsValidIndex(Idx))
	{
		if (UnlockedDialogs[Idx].DialogTree != nullptr)
		{
			if (CurrentDialogTree == UnlockedDialogs[Idx].DialogTree) return;

			CurrentDialogTree = UnlockedDialogs[Idx].DialogTree;
			CurrentAIController->RunBehaviorTree(CurrentDialogTree);

			if (QuestToAnswer != nullptr)
			{
				if (Idx <= 0) return; // we don't want to do this if we display the default Dialog
				uint8 TreeIdx = Dialogs.Find(UnlockedDialogs[Idx]);
				if (TreeIdx != INDEX_NONE)
					if (QuestToAnswer->CountDownObjectiveCharacters(this, TreeIdx))
						QuestToAnswer = nullptr;
			}
		}
	}
}