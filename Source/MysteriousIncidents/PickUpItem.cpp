// Fill out your copyright notice in the Description page of Project Settings.

#include "PickUpItem.h"

#include "Engine/CollisionProfile.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"

#include "Quest.h"
#include "MainCharacter.h"
#include "MCInventoryComponent.h"

// Sets default values
APickUpItem::APickUpItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ItemCollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ItemCollisionCapsule"));
	ItemCollisionCapsule->InitCapsuleSize(10.0f, 10.0f);

	ItemCollisionCapsule->CanCharacterStepUpOn = ECB_No;
	ItemCollisionCapsule->SetShouldUpdatePhysicsVolume(true);
	ItemCollisionCapsule->SetCanEverAffectNavigation(false);
	ItemCollisionCapsule->bDynamicObstacle = false;
	ItemCollisionCapsule->SetNotifyRigidBodyCollision(true);
	ItemCollisionCapsule->SetCollisionProfileName(FName(TEXT("NoCollision")));
	RootComponent = ItemCollisionCapsule;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName(FName(TEXT("NoCollision")));
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCanEverAffectNavigation(false);
}

void APickUpItem::BeginPlay()
{
	Super::BeginPlay();

	if (!ensure(Mesh != nullptr)) return;

	auto MaterialInterface = Mesh->GetMaterial(0);
	if (!ensure(MaterialInterface != nullptr)) return;

	BaseMaterial = MaterialInterface;

	if (IsUnlocked)
		EnableWorldInteraction();
}

void APickUpItem::Unlock()
{
	IsUnlocked = true;

	EnableWorldInteraction();
}

void APickUpItem::Interact(AMainCharacter* MainCharacter)
{
	if (!ensure(MainCharacter != nullptr)) return;
	auto InventoryComponent = MainCharacter->GetInventoryComponent();

	if (!ensure(InventoryComponent != nullptr)) return;

	if (!ensure(QuestToAnswer != nullptr)) return;
	if (QuestToAnswer->CountDownItems(this))
	{
		InventoryComponent->AddNewItem(this);
		DisableVisuals();
		DisableWorldInteraction();
		OnOutOfRange();
	}

}

void APickUpItem::OnInRange()
{
	if (!ensure(Mesh != nullptr)) return;

	Mesh->SetMaterial(0, HoveredMaterial);
}

void APickUpItem::OnOutOfRange()
{
	if (!ensure(Mesh != nullptr)) return;

	Mesh->SetMaterial(0, BaseMaterial);
}

void APickUpItem::DisableVisuals()
{
	if (!ensure(Mesh != nullptr)) return;
	Mesh->SetVisibility(false);
}

void APickUpItem::EnableWorldInteraction()
{
	if (!ensure(ItemCollisionCapsule != nullptr)) return;
	ItemCollisionCapsule->SetCollisionProfileName(TEXT("Interactable"));
}

void APickUpItem::DisableWorldInteraction()
{
	if (!ensure(ItemCollisionCapsule != nullptr)) return;
	ItemCollisionCapsule->SetCollisionProfileName(TEXT("NoCollision"));
}