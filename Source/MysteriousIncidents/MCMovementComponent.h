// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MCMasterActorComponent.h"
#include "MCMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UMCMovementComponent : public UMCMasterActorComponent
{
	GENERATED_BODY()
	
public:
	UMCMovementComponent() { PrimaryComponentTick.bCanEverTick = true; };
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	void SetDirectionX(float Value) { MovementDirection.X = Value; }
	void SetDirectionY(float Value) { MovementDirection.Y = Value; }
	void SetCanSprint(bool bNewCanSprint) { bCanSprint = bNewCanSprint; }

protected:
	FVector MovementDirection{ 0.0f, 0.0f, 0.0f };
	bool bCanSprint = false;
};
