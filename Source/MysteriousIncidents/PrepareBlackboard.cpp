// Fill out your copyright notice in the Description page of Project Settings.


#include "PrepareBlackboard.h"

#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UPrepareBlackboard::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto Blackboard = OwnerComp.GetBlackboardComponent();
	if (!ensure(Blackboard != nullptr)) return EBTNodeResult::Failed;

	if(Blackboard->GetValueAsBool(FName("AlreadyPrepared")) == true)
		return EBTNodeResult::Succeeded;

	Blackboard->SetValueAsBool(FName("False"), false);

	for (uint8 i = 0; i < DialogOptionIDs.Num(); i++)
	{
		FString IString = FString::FromInt(i);
		FString Key = "IsLocked_Quest_" + IString;
		Blackboard->SetValueAsBool(FName(*Key), DialogOptionIDs[i]);
	}

	Blackboard->SetValueAsBool(FName("AlreadyPrepared"), true);

	UE_LOG(LogTemp, Warning, TEXT("Prepared %s"), *Blackboard->GetName());

	return EBTNodeResult::Succeeded;
}