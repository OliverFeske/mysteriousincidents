// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "EndScreenWidget.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UEndScreenWidget : public UMenuWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
		void LoadMainMenu();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_LoadMainMenu;
};