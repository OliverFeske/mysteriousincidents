// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemWidget.h"

#include "Components/TextBlock.h"
#include "Components/Button.h"

#include "InventoryMenuWidget.h"
#include "PickUpItem.h"

void UInventoryItemWidget::Setup(UInventoryMenuWidget* InParent, uint8 Idx, APickUpItem* Item)
{
	Parent = InParent;
	Index = Idx;

	if (!ensure(Item != nullptr)) return;
	if (!ensure(TextBlock_ItemName != nullptr)) return;

	TextBlock_ItemName->SetText(FText::FromString(Item->GetName()));
}

bool UInventoryItemWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Button_SelectItem != nullptr)) { return false; }
	Button_SelectItem->OnClicked.AddDynamic(this, &UInventoryItemWidget::SelectItem);

	return true;
}

void UInventoryItemWidget::SelectItem()
{
	if (!ensure(Parent != nullptr)) return;

	Parent->DisplaySelectedItemInfo(Index);
}