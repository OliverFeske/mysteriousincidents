// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ToggleMenuWidget.h"

#include "InventoryMenuWidget.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UInventoryMenuWidget : public UToggleMenuWidget
{
	GENERATED_BODY()

public:
	void FillScrollBox();
	void DisplaySelectedItemInfo(int Idx);
	virtual void Setup() override;

protected:
	virtual void OnLevelRemovedFromWorld(ULevel* Level, UWorld* World);

private:
	void SetItems();
	void SetObjectiveText();

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_ObjectiveDescription;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_ItemDescription;
	UPROPERTY(meta = (BindWidget))
		class UScrollBox* ScrollBox_InventorySlots;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> InventoryItemClass{ nullptr };

	TArray<class APickUpItem*> Items;
};