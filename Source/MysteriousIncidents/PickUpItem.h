// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Texture.h"

#include "Interactable.h"
#include "Unlockable.h"

#include "PickUpItem.generated.h"

UCLASS()
class MYSTERIOUSINCIDENTS_API APickUpItem : public AActor, public IInteractable, public IUnlockable
{
	GENERATED_BODY()
	
public:
	APickUpItem();

protected:
	virtual void BeginPlay() override;
	
public:
	virtual void Unlock() override;
	virtual void Interact(class AMainCharacter* MainCharacter) override;
	virtual void OnInRange() override;
	virtual void OnOutOfRange() override;
	virtual FString& GetName() override { return ItemName; }
	//FString GetMessage() const { return KeyToPress + Seperator + Action + " " + ItemName; }
	TArray<FString>& GetItemDescription() { return ItemDescription; }
	//class UTexture2D* GetImage() { return Image; }
	bool GetIsUnlocked() { return IsUnlocked; }
	void SetupAnswer(class UQuest* InQuestToAnswer) { QuestToAnswer = InQuestToAnswer; }

private:
	void DisableVisuals();
	void EnableWorldInteraction();
	void DisableWorldInteraction();

	//UPROPERTY(EditAnywhere)
	//	FString KeyToPress{ "E" };
	//UPROPERTY(EditAnywhere)
	//	FString Seperator{ " - " };
	//UPROPERTY(EditAnywhere)
	//	FString Action{ "Pick up" };
	UPROPERTY(EditAnywhere)
		FString ItemName{ "Undefined Item Name" };
	UPROPERTY(EditAnywhere)
		TArray<FString> ItemDescription{ "Needs Description" };
	//UPROPERTY(EditAnywhere)
	//	class UTexture2D* Image {nullptr};
	UPROPERTY()
		class UMaterialInterface* BaseMaterial{ nullptr };
	UPROPERTY()
		class UMaterialInterface* HoveredMaterial{ nullptr };

	UPROPERTY(EditAnywhere)
		class UCapsuleComponent* ItemCollisionCapsule {nullptr};
	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* Mesh {nullptr};

	UPROPERTY()
		bool IsUnlocked{ false };
	UPROPERTY()
		class UQuest* QuestToAnswer{ nullptr };
};