// Fill out your copyright notice in the Description page of Project Settings.

#include "OptionsMenuWidget.h"

#include "Components/Button.h"
#include "Components/Slider.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "MI_CustomUtilities.h"

bool UOptionsMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Button_QuitLevel != nullptr)) return false;
	Button_QuitLevel->OnClicked.AddDynamic(this, &UOptionsMenuWidget::QuitLevel);

	if (!ensure(Button_SaveGame != nullptr)) return false;
	Button_SaveGame->OnClicked.AddDynamic(this, &UOptionsMenuWidget::SaveGame);

	if (!ensure(Slider_Master != nullptr)) return false;
	Slider_Master->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeMasterVolume);

	if (!ensure(Slider_Music != nullptr)) return false;
	Slider_Music->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeMusicVolume);

	if (!ensure(Slider_Ambient != nullptr)) return false;
	Slider_Ambient->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeAmbientVolume);

	if (!ensure(Slider_SFX != nullptr)) return false;
	Slider_SFX->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeSFXVolume);

	if (!ensure(Slider_LookUpSensitivity != nullptr)) return false;
	Slider_LookUpSensitivity->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeLookUpSensitivity);

	if (!ensure(Slider_TurnSensitivity != nullptr)) return false;
	Slider_TurnSensitivity->OnValueChanged.AddDynamic(this, &UOptionsMenuWidget::ChangeTurnSensitivity);

	return true;
}

void UOptionsMenuWidget::OnLevelRemovedFromWorld(ULevel* Level, UWorld* World)
{
	Super::OnLevelRemovedFromWorld(Level, World);

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeUIOnly InputModeData;

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

void UOptionsMenuWidget::QuitLevel()
{
	if (!ensure(MenuInterface != nullptr)) return;

	MenuInterface->LoadLevelAsync("MainMenu");
}

void UOptionsMenuWidget::SaveGame()
{
	if (!ensure(EditableTextBox_SaveGameName != nullptr)) return;

	UE_LOG(LogTemp, Warning, TEXT("Saved Game with name: %s"), *EditableTextBox_SaveGameName->GetText().ToString());
}

void UOptionsMenuWidget::ChangeMasterVolume(float Value)
{
	UGameInstance* GameInstance = GetWorld()->GetGameInstance();
	if (!ensure(GameInstance != nullptr)) return;

	if (!ensure(Text_Master_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_Master_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed Master Volume to: %s"), *NewText.ToString());
}

void UOptionsMenuWidget::ChangeMusicVolume(float Value)
{
	UGameInstance* GameInstance = GetWorld()->GetGameInstance();
	if (!ensure(GameInstance != nullptr)) return;

	if (!ensure(Text_Music_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_Music_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed Music Volume to: %s"), *NewText.ToString());
}

void UOptionsMenuWidget::ChangeAmbientVolume(float Value)
{
	UGameInstance* GameInstance = GetWorld()->GetGameInstance();
	if (!ensure(GameInstance != nullptr)) return;

	if (!ensure(Text_Ambient_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_Ambient_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed Ambient Volume to: %s"), *NewText.ToString());
}

void UOptionsMenuWidget::ChangeSFXVolume(float Value)
{
	UGameInstance* GameInstance = GetWorld()->GetGameInstance();
	if (!ensure(GameInstance != nullptr)) return;

	if (!ensure(Text_SFX_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_SFX_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed SFX Volume to: %s"), *NewText.ToString());
}

void UOptionsMenuWidget::ChangeLookUpSensitivity(float Value)
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	if (!ensure(Text_LookUp_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_LookUp_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed Look Up Sensitivity to: %s"), *NewText.ToString());
}

void UOptionsMenuWidget::ChangeTurnSensitivity(float Value)
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	if (!ensure(Text_Turn_Num != nullptr)) return;
	FText NewText = MI_CustomUtilities::FormatFloatText(Value, 1);
	Text_Turn_Num->SetText(NewText);

	UE_LOG(LogTemp, Warning, TEXT("Changed Turn Sensitivity to: %s"), *NewText.ToString());
}