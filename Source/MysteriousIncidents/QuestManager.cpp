// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestManager.h"

#include "Kismet/GameplayStatics.h"

#include "MysteriousIncidentsGameInstance.h"
#include "Quest.h"

// Sets default values
AQuestManager::AQuestManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AQuestManager::BeginPlay()
{
	Super::BeginPlay();
}

void AQuestManager::CreateQuest(FQuestBase& RawQuest)
{
	UQuest* NewQuest = NewObject<UQuest>();
	if (!ensure(NewQuest != nullptr)) return;

	auto GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return;

	auto Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return;

	NewQuest->Setup(RawQuest, Inst);

	ActiveQuests.Add(NewQuest);
}

void AQuestManager::FinishedQuest(UQuest* InFinishedQuest)
{
	if (!ensure(InFinishedQuest != nullptr)) return;

	FinishedQuests.Add(InFinishedQuest);
	ActiveQuests.Remove(InFinishedQuest);

	UE_LOG(LogTemp, Warning, TEXT("Finished %s"), *InFinishedQuest->GetQuestName());
}