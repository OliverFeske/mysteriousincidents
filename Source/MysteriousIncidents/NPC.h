// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MysteriousIncidentsCharacter.h"
#include "Interactable.h"
#include "StructCollection.h"

#include "NPC.generated.h"

UCLASS()
class MYSTERIOUSINCIDENTS_API ANPC : public ACharacter, public IInteractable
{
	GENERATED_BODY()

public:
	ANPC();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Interact(class AMainCharacter* MainCharacter) override;
	virtual void OnInRange() override {}
	virtual void OnOutOfRange() override {}
	virtual FString& GetName() override { return Name; }
	void SetCharacterID(uint8 ID) { CharacterID = ID; }
	class UBehaviorTree* GetCurrentDialogTree() { return CurrentDialogTree; }
	void SetNPCManager(class ANPCManager* InNPCManager) { NPCManager = InNPCManager; }
	class ANPCManager* GetNPCManager() { return NPCManager; }
	void RunCurrentBehaviorTree(uint8 Idx);
	void UnlockDialog(uint8 Idx);
	void SetUpAnswer(class UQuest* InQuestToAnswer) { QuestToAnswer = InQuestToAnswer; }
	TArray<FDialogStruct>& GetUnlockedDialogs() { return UnlockedDialogs; }

protected:
	virtual void DisplayInfo() const {}

	UPROPERTY(EditAnywhere)
		TArray<FDialogStruct> Dialogs;
	UPROPERTY(VisibleAnywhere)
		TArray<FDialogStruct> UnlockedDialogs;
	UPROPERTY(EditAnywhere)
		FString Name {"Unnamed!"};
	UPROPERTY(EditAnywhere)
		FString Seperator {" - "};
	UPROPERTY(EditAnywhere)
		FString InfoMessage {"Put info here!"};

	UPROPERTY(VisibleAnywhere)
		class AAIController* CurrentAIController{ nullptr };
	UPROPERTY(VisibleAnywhere)
		class UBehaviorTree* CurrentDialogTree{ nullptr };
	UPROPERTY(VisibleAnywhere)
		class UCapsuleComponent* CharacterCollisionCapsule{ nullptr };
	UPROPERTY(VisibleAnywhere)
		uint8 CharacterID;
	UPROPERTY(VisibleAnywhere)
		class ANPCManager* NPCManager{ nullptr };

	class UQuest* QuestToAnswer{ nullptr };
};	