// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ToggleMenuWidget.h"

#include "OptionsMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UOptionsMenuWidget : public UToggleMenuWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;
	virtual void OnLevelRemovedFromWorld(ULevel* Level, UWorld* World);

private:
	UFUNCTION()
		void QuitLevel();
	UFUNCTION()
		void SaveGame();
	UFUNCTION()
		void ChangeMasterVolume(float Value);
	UFUNCTION()
		void ChangeMusicVolume(float Value);
	UFUNCTION()
		void ChangeAmbientVolume(float Value);
	UFUNCTION()
		void ChangeSFXVolume(float Value);
	UFUNCTION()
		void ChangeLookUpSensitivity(float Value);
	UFUNCTION()
		void ChangeTurnSensitivity(float Value);

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_QuitLevel;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_SaveGame;
	UPROPERTY(meta = (BindWidget))
		class UEditableTextBox* EditableTextBox_SaveGameName;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_Master;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_Music;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_Ambient;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_SFX;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_LookUpSensitivity;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_TurnSensitivity;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_Master_Num;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_Music_Num;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_Ambient_Num;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_SFX_Num;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_LookUp_Num;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Text_Turn_Num;
};
