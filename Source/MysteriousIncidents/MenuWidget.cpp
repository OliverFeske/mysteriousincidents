// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"

#include "MenuInterface.h"

void UMenuWidget::SetMenuInterface(IMenuInterface* IMenu)
{
	MenuInterface = IMenu;
}

void UMenuWidget::Setup()
{
	this->bIsFocusable = true;
	this->AddToViewport();

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (!ensure(PC != nullptr)) return;
	
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PC->SetInputMode(InputModeData);
	PC->bShowMouseCursor = true;
}