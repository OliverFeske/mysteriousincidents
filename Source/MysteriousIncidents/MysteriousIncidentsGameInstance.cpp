// Fill out your copyright notice in the Description page of Project Settings.

#include "MysteriousIncidentsGameInstance.h"

#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

#include "MainCharacter.h"
#include "OptionsMenuWidget.h"
#include "InventoryMenuWidget.h"
#include "DialogMenuWidget.h"
#include "MainMenuWidget.h"
#include "EndScreenWidget.h"

void UMysteriousIncidentsGameInstance::LoadMainMenuWidget()
{
	if (!ensure(MainMenuClass != nullptr)) return;

	MainMenu = CreateWidget<UMainMenuWidget>(this, MainMenuClass);
	if (!ensure(MainMenu != nullptr)) return;

	MainMenu->Setup();
	MainMenu->SetMenuInterface(this);
}

void UMysteriousIncidentsGameInstance::LoadEndScreenWidget()
{
	if (!ensure(EndScreenClass != nullptr)) return;

	EndScreen = CreateWidget<UEndScreenWidget>(this, EndScreenClass);
	if (!ensure(EndScreen != nullptr)) return;

	EndScreen->Setup();
	EndScreen->SetMenuInterface(this);
}

void UMysteriousIncidentsGameInstance::OpenOptionsMenuWidget() { CreateOptionsMenuWidget(); }
void UMysteriousIncidentsGameInstance::OpenInventoryMenuWidget() { CreateInventoryMenuWidget(); }

void UMysteriousIncidentsGameInstance::CreateOptionsMenuWidget() 
{ 
	auto ToggleWidget = CreateToggleMenuWidget(OptionsMenu, OptionsMenuClass);
	if (!ensure(ToggleWidget != nullptr)) return;

	auto OptionsWidget = Cast<UOptionsMenuWidget>(ToggleWidget);
	if (!ensure(OptionsWidget != nullptr)) return;

	OptionsMenu = OptionsWidget;
}

void UMysteriousIncidentsGameInstance::CreateInventoryMenuWidget() 
{ 
	auto ToggleWidget = CreateToggleMenuWidget(InventoryMenu, InventoryMenuClass);
	if (!ensure(ToggleWidget != nullptr)) return;

	auto InventoryWidget = Cast<UInventoryMenuWidget>(ToggleWidget);
	if (!ensure(InventoryWidget != nullptr)) return;

	InventoryMenu = InventoryWidget;
}

void UMysteriousIncidentsGameInstance::CreateDialogMenuWidget() 
{ 
	auto ToggleWidget = CreateToggleMenuWidget(DialogMenu, DialogMenuClass);
	if (!ensure(ToggleWidget != nullptr)) return;

	auto DialogWidget = Cast<UDialogMenuWidget>(ToggleWidget);
	if (!ensure(DialogWidget != nullptr)) return;

	DialogMenu = DialogWidget;
}

UToggleMenuWidget* UMysteriousIncidentsGameInstance::CreateToggleMenuWidget(UToggleMenuWidget* MemberPtr, TSubclassOf<UToggleMenuWidget> ToggleMenuClass)
{
	if (!ensure(ToggleMenuClass != nullptr)) return nullptr;

	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return nullptr;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return nullptr;

	MemberPtr = CreateWidget<UToggleMenuWidget>(Inst, ToggleMenuClass);
	if (!ensure(MemberPtr != nullptr)) return nullptr;

	MemberPtr->SetMenuInterface(Inst);
	MemberPtr->Setup();

	return MemberPtr;
}

void UMysteriousIncidentsGameInstance::LoadLoadingWidget()
{
	auto LoadingWidget = CreateWidget<UUserWidget>(GetWorld(), LoadingScreenClass);
	if (LoadingWidget == nullptr) return;

	LoadingWidget->bIsFocusable = true;
	LoadingWidget->AddToViewport();
}

//void UMysteriousIncidentsGameInstance::LoadMainMenuLevel()
//{
//	CurrentLoadingLevelName = "/Game/Maps/MainMenu";
//
//	StartLoadingPackage();
//}

//void UMysteriousIncidentsGameInstance::LoadMainLevel(FString LevelName)
//{
//	// TODO: CHECK FOR NEW GAME OR IF NOT EMPTY STRING THEN LOAD SAVED GAME AFTER TRAVELING TO THE MAP
//	if (LevelName == "")
//		LevelName = "/Game/Maps/Island";
//	//else
//		// LOAD MAP BUT ASSIGN THE NAME TO THE INTERFACE FOR TRAVELING AND KEEPING THE INFORMATION
//
//	LevelName = TEXT("/Game/Maps/Island");
//
//	CurrentLoadingLevelName = LevelName;
//
//	StartLoadingPackage();
//}

void UMysteriousIncidentsGameInstance::LoadLevelAsync(FString LevelName)
{
	CurrentLoadingLevelName = TEXT("/Game/Maps/" + LevelName);

	LoadLoadingWidget();
	StartLoadingPackage();
}

void UMysteriousIncidentsGameInstance::StartLoadingPackage()
{
	if (bIsCurrentlyLoading)
	{
		UE_LOG(LogTemp, Error, TEXT("Already loading something"));
		return;
	}

	bIsCurrentlyLoading = true;

	LoadPackageAsync(
		CurrentLoadingLevelName,
		FLoadPackageAsyncDelegate::CreateLambda([=](const FName& PackageName, UPackage* LoadedPackage, EAsyncLoadingResult::Type Result)
			{
				if (Result == EAsyncLoadingResult::Succeeded)
					OnLoadPackageSucceeded();
			}),
		0,
				PKG_ContainsMap);

	UE_LOG(LogTemp, Warning, TEXT("Started async loading package ( %s )"), *CurrentLoadingLevelName);
}

void UMysteriousIncidentsGameInstance::OnLoadPackageSucceeded()
{
	GetWorld()->GetTimerManager().SetTimer(hLoadingTimer, this, &UMysteriousIncidentsGameInstance::OnPreheatFinished, 3.0f, false);

	UE_LOG(LogTemp, Warning, TEXT("Started timer for preheating the level"));
}

void UMysteriousIncidentsGameInstance::OnPreheatFinished()
{
	UE_LOG(LogTemp, Warning, TEXT("Opening level"));

	bIsCurrentlyLoading = false;

	UGameplayStatics::OpenLevel(GetWorld(), *CurrentLoadingLevelName);
}