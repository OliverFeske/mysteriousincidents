// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ToggleMenuWidget.h"
#include "StructCollection.h"

#include "DialogMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UDialogMenuWidget : public UToggleMenuWidget
{
	GENERATED_BODY()
	
public:
	void SetNPC(class ANPC* IC);
	void DisplayDialog(uint8 Idx);
	void SetText(FString& NewText);

private:
	void FillDialogList();

	UPROPERTY(meta = (BindWidget))
		class UScrollBox* ScrollBox_DialogList;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_DialogText;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_CharacterName;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> DialogListItemClass;

	UPROPERTY()
		class ANPC* TalkingNPC{ nullptr };

	UPROPERTY()
		TArray<FDialogStruct> Dialogs;
};
