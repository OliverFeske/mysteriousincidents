// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MYSTERIOUSINCIDENTS_API IMenuInterface
{
	GENERATED_BODY()

public:
	virtual void LoadLevelAsync(FString LevelName) = 0;
	//virtual void LoadMainMenuLevel() = 0;
	//virtual void LoadMainLevel(FString LevelName) = 0;
	//virtual void LoadEndScreenLevel(FString LevelName) = 0;
};
